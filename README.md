# Build

    znc-buildmod fish.cpp
    cp fish.so /usr/local/lib/znc or ~/.znc/modules

# Usage

    /msg *fish help

## show config

    listconfig

## prefix

    SetConfig prefix_decrypted
    SetConfig prefix_encrypted
